package edu.ktu.p175b014.gsm.templates;
//     |       |        |   |         |

/**
 * Stiliaus gidas paprastai tarnybinei klasei.
 * 
 * N.B. JavaDoc aprašą derėtų formatuoti naudojant HTML žymas, tačiau
 * čia nusižengiu taisyklėms ir naudoju Markdown sintaksę, kad komentarai
 * būtų lengviau skaitomi išeities tekstuose.
 * 
 * 1. Paketo pavadinimas:
 *  * Mažosiomis raidėmis. Pirma didžioji rodo klasę.
 *  * Unikalus prefiksas + "." + autorius + "." + gaminys + "." komponentas.
 *    Studijų reikalams tiktų:
 *    a) "edu.ktu" (KTU hostname) +
 *       .{modulio kodas}.{nick}.{užduotis}.
 *       Pvz:
 * <code>
 *          edu.ktu.p123b123.inc0gnit0.lab1
 *         | {org} |{group} | {nick}  |    |
 * </code>
 * 
 *    -- arba --
 *    b) "edu.ktu" +
 *       .{grupės pavadinimas mažosiomis , pvz. ifxy08_03}.{nick}.{užduotis}.
 *       Pvz:
 * <code>
 *          edu.ktu.ifxy8_23.inc0gnit0.lab1
 *         |       |        |         |    |
 * </code>
 * 
 *    c) JOKIŲ DIDŽIŲJŲ ir non-ASCII raidžių (žr. projektus ÇaVa bei Bonjour).
 *       net tokiu atveju: ktu.edu.ifxy8_23.iamablackhat.myinfamoushacks.
 * 
 * 2. Viename faile – viena <code>public</code> klasė, kurios pavadinimas sutampa su
 *    failo pavadinimu, kelias nuo projekto šaknies – su paketu (to reikalauja
 *    javac).
 * 
 * 3. Tiek paketo, tiek klasės pavadinimas turi kuo griežčiau apibrėžti
 *    naudojimo sritį.
 * 
 * Tarkime, šios klasės pilnas pavadinimas:
 * <code>
 *      ed+u.ktu.p175b014.gsm.templates.TemplateClass
 *     |  org  |gr/proj |aš |  tema   |    KLASĖ    |
 *     |    UNIKALUS PILNAS PAKETAS   |             |
 * </code>
 * 
 * 4. Tam, kad vizualiai atskirtume klasę nuo paketo, klasę pavadinsime  
 *    IšDidžiosiosRaidėsKuPraNuGaRišKai.
 * 
 *    N.B. kompiliatoriui raidžių registras nesvarbu, todėl jis priims 
 * @author gsm, mindaugas.smolinskas@ktu.lt
 */
public class TemplateClass {
    // Tuščia demo klasė: nekreipti dėmesio į pranešimus: šablonas yra
    // Gsm.java.
    //
    // Geri IDE teisingai pyksta: klasė, neturinti laukų, metodų ir kitų
    // realizacijos detalių, turėtų būti paversta interfeisu.
}
