package LaboraiDemo;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Scanner;
import studijosKTU.*;

/**
 * @author EK
 */
public final class Automobilis implements KTUable {

    // bendri duomenys visiems automobiliams (visai klasei)
    static private int priimtinųMetųRiba = 1990;
    static private int dabartiniaiMetai = 2010;
    static private double mažiausiaKaina = 100.0;
    static private double didžiausiaKaina = 333000.0;
    private String marke = "";
    private String modelis = "";
    private int gamMetai = -1;
    private int rida = -1;
    private double kaina = -1.0;

    public Automobilis() {
    }

    public Automobilis(String markė, String modelis,
            int gamMetai, int rida, double kaina) {
        this.marke = markė;
        this.modelis = modelis;
        this.gamMetai = gamMetai;
        this.rida = rida;
        this.kaina = kaina;
        validate();
    }

    public Automobilis(String e) {
        this.fromString(e);
    }

    @Override
    public Automobilis create(String dataString) {
        return new Automobilis(dataString);
    }

    @Override
    public String validate() {
        String klaidosTipas = "";
        assert (gamMetai < priimtinųMetųRiba || gamMetai > dabartiniaiMetai) :
                klaidosTipas = "Blogai nurodyti gamybos metai; ";
        assert (kaina < mažiausiaKaina || kaina > didžiausiaKaina) :
                klaidosTipas += "Kaina už leistinų ribų; ";
        return klaidosTipas;
    }

    @Override
    public void fromString(String autoD) {
        try {   // ed - tai elementarūs duomenys, atskirti tarpais
            Scanner ed = new Scanner(autoD);
            marke = ed.next();
            modelis = ed.next();
            gamMetai = ed.nextInt();
            setRida(ed.nextInt());
            setKaina(ed.nextDouble());
            validate();
        } catch (InputMismatchException e) {
            Ks.ern("Blogas duomenų formatas apie auto -> " + autoD);
        } catch (NoSuchElementException e) {
            Ks.ern("Trūksta duomenų apie auto -> " + autoD);
        }
    }

    @Override
    public String toString() {
        return marke + "_" + modelis + ":" + gamMetai + " " + getRida() + " "
                + String.format("%4.1f", kaina);
    }

    public String getMarkė() {
        return marke;
    }

    public String getModelis() {
        return modelis;
    }

    public int getGamMetai() {
        return gamMetai;
    }

    public int getRida() {
        return rida;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }

    public void setRida(int rida) {
        this.rida = rida;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.marke);
        hash = 53 * hash + Objects.hashCode(this.modelis);
        hash = 53 * hash + this.gamMetai;
        hash = 53 * hash + this.rida;
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.kaina) ^ (Double.doubleToLongBits(this.kaina) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Automobilis other = (Automobilis) obj;
        if (!Objects.equals(this.marke, other.marke)) {
            return false;
        }
        if (!Objects.equals(this.modelis, other.modelis)) {
            return false;
        }
        if (this.gamMetai != other.gamMetai) {
            return false;
        }
        if (this.rida != other.rida) {
            return false;
        }
        if (Double.doubleToLongBits(this.kaina) != Double.doubleToLongBits(other.kaina)) {
            return false;
        }
        return true;
    }
}
