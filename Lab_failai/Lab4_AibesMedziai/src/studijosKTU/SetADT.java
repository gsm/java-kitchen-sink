package studijosKTU;

//Interfeisu aprašomas Aibės ADT.
public interface SetADT<E> extends Iterable<E> {

    /**
     * Patikrinama ar aibė tuščia.
     *
     * @return Grąžinama true, jei aibė tuščia.
     */
    boolean isEmpty();

    /**
     * Grąžinamas aibėje esančių elementų kiekis.
     *
     * @return Grąžinamas aibėje esančių elementų kiekis.
     */
    int size();

    /**
     * Išvaloma aibė.
     */
    void clear();

    /**
     * Aibė papildoma nauju elementu ir grąžinama true.
     *
     * @param e - Aibės elementas.
     * @return Aibė papildoma nauju elementu e ir grąžinama true.
     */
    boolean add(E e);

    /**
     * Pašalinamas elementas iš aibės.
     *
     * @param e - Aibės elementas.
     * @return Gražinama true, pašalinus elementą e iš aibės.
     */
    boolean remove(E e);

    /**
     * Patikrinama ar aibėje egzistuoja elementas.
     *
     * @param e - Aibės elementas.
     * @return Grąžinama true, jei aibėje egzistuoja elementas e.
     */
    boolean contains(E e);

    /**
     * Grąžinamas aibės elementų masyvas.
     *
     * @return Grąžinamas aibės elementų masyvas.
     */
    Object[] toArray();
}
