package studijosKTU;

import java.util.*;

public interface SortedSetADT<E> extends SetADT<E> {

    /**
     * Grąžinamas aibės poaibis iki elemento e.
     *
     * @param e - Aibės elementas.
     * @return Grąžinamas aibės poaibis iki elemento e.
     */
    SetADT<E> headSet(E e);

    /**
     * Grąžinamas aibės poaibis nuo elemento e1 iki e2.
     *
     * @param e1 - pradinis aibės poaibio elementas.
     * @param e2 - galinis aibės poaibio elementas.
     * @return Grąžinamas aibės poaibis nuo elemento e1 iki e2.
     */
    SetADT<E> subSet(E e1, E e2);

    /**
     * Grąžinamas aibės poaibis iki elemento e.
     *
     * @param e - Aibės elementas.
     * @return Grąžinamas aibės poaibis nuo elemento e.
     */
    SetADT<E> tailSet(E e);

    /**
     * Grąžinamas atvirkštinis iteratorius.
     *
     * @return Grąžinamas atvirkštinis iteratorius.
     */
    Iterator<E> descendingIterator();
}
