package studijosKTU;

import java.util.Comparator;

public class BstSetKTUx2<E extends KTUable<E>> extends BstSetKTUx<E>
        implements SortedSetADT<E> {
    
    public BstSetKTUx2(E baseObj) {
        super(baseObj);
    }
    
    public BstSetKTUx2(E baseObj, Comparator<E> c) {
        super(baseObj, c);
        this.c = c;
    }

//==============================================================================
// Aibė papildoma nauju elementu. Papildymas atliekamas iteracijos į gylį būdu;
//==============================================================================
    @Override
    public boolean add(E element) {
        if (element == null) {
            return false;
        }
        if (getRoot() == null) {
            setRoot(new BstNode<>(element));
            setSize(size() + 1);
            return true;
        }
        BstNode<E> n = get(element);
        int cmp = (c == null)
                ? super.getComparable(element).compareTo(n.element)
                : c.compare(element, n.element);
        if (cmp < 0) {
            n.left = new BstNode<>(element);
        } else if (cmp > 0) {
            n.right = new BstNode<>(element);
        } else {
            return false;
        }
        setSize(size() + 1);
        return true;
    }
}
