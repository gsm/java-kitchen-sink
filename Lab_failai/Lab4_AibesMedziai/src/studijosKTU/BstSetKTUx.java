package studijosKTU;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;

public class BstSetKTUx<E extends KTUable<E>> extends BstSetKTU<E>
        implements SortedSetADTx<E> {

    private final E baseObj;       // bazinis objektas skirtas naujų kūrimui

    public BstSetKTUx(E baseObj) { // konstruktorius su bazinio objekto
        super();
        this.baseObj = baseObj;     // fiksacija dėl naujų elementų kūrimo
    }

    public BstSetKTUx(E baseObj, Comparator<E> c) { // konstruktorius su bazinio objekto
        super(c);
        this.baseObj = baseObj;     // fiksacija dėl naujų elementų kūrimo
    }

    @Override
    public void add(String dataString) {        // sukuria elementą iš String
        super.add((E) baseObj.create(dataString)); // ir įdeda jį į pabaigą
    }

    @Override
    public void load(String fName) {//suformuoja sąrašą iš fName failo
        clear();
        if (fName.length() == 0) {
            return;
        }
        if (baseObj == null) {          // elementų kūrimui reikalingas baseObj
            Ks.ern("Naudojant load-metodą, "
                    + "reikia taikyti konstruktorių = new SetBstKTUx(new Data())");
        }
        try {
            (new File(Ks.getDataFolder())).mkdir();
            String fN = Ks.getDataFolder() + File.separatorChar + fName;
            try (BufferedReader fReader = new BufferedReader(new FileReader(new File(fN)))) {
                String dLine;
                while ((dLine = fReader.readLine()) != null) {
                    add(dLine);
                }
            }
        } catch (FileNotFoundException e) {
            Ks.ern("Tinkamas duomenų failas " + fName + " nerastas");
        } catch (IOException e) {
            Ks.ern("Failo " + fName + " skaitymo klaida");
        }
    }
}