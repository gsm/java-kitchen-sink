package studijosKTU;

public interface SortedSetADTx<E> extends SortedSetADT<E> {

    public void add(String dataString);

    public void load(String fName);

    public String toVisualizedString(String treeDrawType, String dataCodeDelimiter);

    public Object clone() throws CloneNotSupportedException;
}