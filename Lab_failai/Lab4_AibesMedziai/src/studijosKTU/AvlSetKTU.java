package studijosKTU;

import java.util.Comparator;

/**
 * Rikiuojamos objektų kolekcijos - aibės realizacija AVL-medžiu.
 *
 * @param <E> Aibės elementas, turi tenkinti interfeisą Comparable, arba per
 * klasės konstruktorių turi būti paduodamas Comparator klasės objektas.
 * @užduotis Peržiūrėkite ir išsiaiškinkite pateiktus metodus.
 *
 * @author darius.matulis@ktu.lt
 */
public class AvlSetKTU<E> extends BstSetKTU<E>
        implements SortedSetADT<E> {

    public AvlSetKTU() {
    }

    public AvlSetKTU(Comparator<E> c) {
        super(c);
        this.c = c;
    }

    /**
     * Aibė papildoma nauju elementu ir grąžinama true.
     *
     * @return Aibė papildoma nauju elementu ir grąžinama true.
     */
    @Override
    public boolean add(E element) {
        if (element == null) {
            return false;
        }
        setRoot(addRecursive(element, (AVLNode<E>) getRoot()));
        if (!returned) {
            returned = true;
            return false;
        }
        setSize(size() + 1);
        return true;
    }

//==============================================================================
// Privatus rekursinis metodas naudojamas add metode;
//==============================================================================
    private AVLNode<E> addRecursive(E element, AVLNode<E> n) {
        if (n == null) {
            return new AVLNode<>(element);
        }
        int cmp = (c == null)
                ? super.getComparable(element).compareTo(n.element)
                : c.compare(element, n.element);
        if (cmp < 0) {
            n.setLeft(addRecursive(element, n.getLeft()));
            if ((height(n.getLeft()) - height(n.getRight())) == 2) {
                int cmp2 = (c == null)
                        ? super.getComparable(element).compareTo(n.left.element)
                        : c.compare(element, n.left.element);
                n = (cmp2 < 0) ? rightRotation(n) : doubleRightRotation(n);
            }
        } else if (cmp > 0) {
            n.right = addRecursive(element, n.getRight());
            if ((height(n.getRight()) - height(n.getLeft())) == 2) {
                int cmp2 = (c == null)
                        ? super.getComparable(n.right.element).compareTo(element)
                        : c.compare(n.right.element, element);
                n = (cmp2 < 0) ? leftRotation(n) : doubleLeftRotation(n);
            }
        } else {
            returned = false;
        }
        n.height = Math.max(height(n.getLeft()), height(n.getRight())) + 1;
        return n;
    }

    /**
     * Pašalinamas elementas iš aibės.
     *
     * @return Gražinama true, pašalinus elementą iš aibės.
     */
    @Override
    public boolean remove(E element) {
        throw new UnsupportedOperationException("Studentams reikia realizuoti remove()");
    }

    private AVLNode<E> removeRecursive(E element, AVLNode<E> n) {
        throw new UnsupportedOperationException("Studentams reikia realizuoti removeRecursive()");
    }

//==============================================================================
// Papildomi privatūs metodai, naudojami operacijų su aibe realizacijai
// AVL-medžiu;
//==============================================================================
//==============================================================================
//        n2
//       /                n1
//      n1      ==>      /  \
//     /                n3  n2
//    n3
//==============================================================================
    private AVLNode<E> rightRotation(AVLNode<E> n2) {
        AVLNode<E> n1 = n2.getLeft();
        n2.setLeft(n1.getRight());
        n1.setRight(n2);
        n2.height = Math.max(height(n2.getLeft()), height(n2.getRight())) + 1;
        n1.height = Math.max(height(n1.getLeft()), height(n2)) + 1;
        return n1;
    }

    private AVLNode<E> leftRotation(AVLNode<E> n1) {
        AVLNode<E> n2 = n1.getRight();
        n1.setRight(n2.getLeft());
        n2.setLeft(n1);
        n1.height = Math.max(height(n1.getLeft()), height(n1.getRight())) + 1;
        n2.height = Math.max(height(n2.getRight()), height(n1)) + 1;
        return n2;
    }

//==============================================================================
//        n3               n3
//       /                /                n2
//      n1      ==>      n2      ==>      /  \
//       \              /                n1  n3
//        n2           n1
//============================================================================== 
    private AVLNode<E> doubleRightRotation(AVLNode<E> n3) {
        n3.left = leftRotation(n3.getLeft());
        return rightRotation(n3);
    }

    private AVLNode<E> doubleLeftRotation(AVLNode<E> n1) {
        n1.right = rightRotation(n1.getRight());
        return leftRotation(n1);
    }

    private int height(AVLNode<E> n) {
        return (n == null) ? -1 : n.height;
    }

//==============================================================================
//Vidinė kolekcijos mazgo klasė
//==============================================================================
    class AVLNode<E> extends BstNode<E> {

        int height;

        AVLNode(E element) {
            super(element);
            this.height = 0;
        }

        public void setLeft(AVLNode<E> left) {
            super.left = (BstNode) left;
        }

        public AVLNode<E> getLeft() {
            return (AVLNode<E>) left;
        }

        public void setRight(AVLNode<E> right) {
            super.right = (BstNode) right;
        }

        public AVLNode<E> getRight() {
            return (AVLNode<E>) right;
        }
    }
}
