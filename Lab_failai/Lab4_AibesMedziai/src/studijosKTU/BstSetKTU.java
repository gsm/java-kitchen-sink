package studijosKTU;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Stack;

/**
 * Rikiuojamos objektų kolekcijos - aibės realizacija dvejetainiu paieškos
 * medžiu.
 *
 * @param <E> Aibės elementas, turi tenkinti interfeisą Comparable, arba per
 * klasės konstruktorių turi būti paduodamas Comparator klasės objektas.
 *
 * @užduotis Peržiūrėkite ir išsiaiškinkite pateiktus metodus.
 *
 * @author darius.matulis@ktu.lt
 */
public class BstSetKTU<E> implements SortedSetADT<E>, Cloneable {

    //Medžio šaknies mazgas
    private BstNode<E> root = null;
    //Medžio dydis
    private int size = 0;
    //Kintamasis, nurodantis ar pavyko operacija su aibe
    boolean returned = true;
    //Rodyklė į komparatorių. Jei c = null, tada naudojamas 
    //Comparable<T>
    Comparator<E> c = null;

    //Sukuriamas aibės objektas DP-medžio raktams naudojant Comparable<T>
    public BstSetKTU() {
    }

    //Sukuriamas aibės objektas DP-medžio raktams naudojant Comparator<T>
    public BstSetKTU(Comparator<E> c) {
        this.c = c;
    }

    /**
     * Patikrinama ar aibė tuščia.
     *
     * @return Grąžinama true, jei aibė tuščia.
     */
    @Override
    public boolean isEmpty() {
        return root == null;
    }

    /**
     * @return Grąžinamas aibėje esančių elementų kiekis.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Išvaloma aibė.
     */
    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    /**
     * Patikrinama ar aibėje egzistuoja elementas.
     *
     * @param element - Aibės elementas.
     * @return Grąžinama true, jei aibėje egzistuoja elementas element.
     */
    @Override
    public boolean contains(E element) {
        if (element == null) {
            throw new IllegalArgumentException("element is null in contains(E element)");
        }

        boolean result = false;
        if (root != null) {
            if (c == null) {
                Comparable<E> comparable = getComparable(get(element).element);
                result = comparable.compareTo(element) == 0;
            } else {
                result = c.compare(get(element).element, element) == 0;
            }
        }
        return result;
    }

    /**
     * Aibė papildoma nauju elementu ir grąžinama true.
     *
     * @param element - Aibės elementas.
     * @return Aibė papildoma nauju elementu ir grąžinama true.
     */
    @Override
    public boolean add(E element) {
        boolean result = false;
        if (element != null) {
            root = addRecursive(element, root);
            //Jei elementas egzistuoja aibėje, grąžinama false
            if (!returned) {
                returned = true;
            } else {
                size++;
                result = true;
            }
        }
        return result;
    }

    /**
     * Pašalinamas elementas iš aibės.
     *
     * @param element - Aibės elementas.
     * @return Gražinama true, pašalinus elementą iš aibės.
     */
    @Override
    public boolean remove(E element) {
        boolean result = false;
        if (element != null) {
            root = removeRecursive(element, root);
            //Jei nerasta ką pašalinti, grąžinama false
            if (!returned) {
                returned = true;
            } else {
                size--;
                result = true;
            }
        }
        return result;
    }

//==============================================================================
// Papildomi metodai, naudojami operacijų su aibe realizacijai
// dvejetainiu paieškos medžiu;
//==============================================================================
    Comparable<E> getComparable(E element) {
        if (element instanceof Comparable) {
            return (Comparable<E>) element;
        } else {
            throw new IllegalArgumentException("Element object does not implements Comparable");
        }
    }

    private BstNode<E> addRecursive(E element, BstNode<E> n) {
        if (n == null) {
            return new BstNode<>(element);
        }
        int cmp = (c == null)
                ? getComparable(element).compareTo(n.element)
                : c.compare(element, n.element);
        if (cmp < 0) {
            n.left = addRecursive(element, n.left);
        } else if (cmp > 0) {
            n.right = addRecursive(element, n.right);
        } else {
            returned = false;
        }
        return n;
    }

    private BstNode<E> removeRecursive(E element, BstNode<E> n) {
        if (n == null) {
            returned = false;
            return n;
        }
        //Medyje ieškomas šalinamas elemento mazgas;
        int cmp = (c == null)
                ? getComparable(element).compareTo(n.element)
                : c.compare(element, n.element);
        if (cmp < 0) {
            n.left = removeRecursive(element, n.left);
        } else if (cmp > 0) {
            n.right = removeRecursive(element, n.right);
        } else if (n.left != null && n.right != null) {
            /*Atvejis kai šalinamas elemento mazgas turi abu vaikus.
             Ieškomas didžiausio rakto elemento mazgas kairiajame pomedyje.
             Galima kita realizacija kai ieškomas mažiausio rakto
             elemento mazgas dešiniajame pomedyje. Tam yra sukurtas
             metodas getMin(E element);
             */
            BstNode<E> nodeMax = getMax(n.left);
            /*Didžiausio rakto elementas (TIK DUOMENYS!) perkeliamas į šalinamo
             elemento mazgą. Pats mazgas nėra pašalinamas - tik atnaujinamas;
             */
            n.element = nodeMax.element;
            //Surandamas ir pašalinamas maksimalaus rakto elemento mazgas;
            n.left = removeMax(n.left);
            //Kiti atvejai
        } else {
            n = (n.left != null) ? n.left : n.right;
        }
        return n;
    }

    //Grąžina elemento mazgą. Naudojamas contains metode;
    BstNode<E> get(E element) {
        if (element == null) {
            throw new IllegalArgumentException("element is null in get(E element)");
        }
        BstNode<E> n = root;
        BstNode<E> nParent = null;
        while (n != null) {
            nParent = n;
            int cmp = (c == null)
                    ? getComparable(element).compareTo(n.element)
                    : c.compare(element, n.element);
            if (cmp < 0) {
                n = n.left;
            } else if (cmp > 0) {
                n = n.right;
            } else {
                return n;
            }
        }
        return nParent;
    }

    //Pašalina maksimalaus rakto elementą paiešką pradedant mazgu node
    BstNode<E> removeMax(BstNode<E> n) {
        if (n == null) {
            return null;
        } else if (n.right != null) {
            n.right = removeMax(n.right);
            return n;
        } else {
            return n.left;
        }
    }

    //Grąžina maksimalaus rakto elementą paiešką pradedant mazgu node
    BstNode<E> getMax(BstNode<E> n) {
        return get(n, true);
    }

    //Grąžina minimalaus rakto elementą paiešką pradedant mazgu node
    BstNode<E> getMin(BstNode<E> n) {
        return get(n, false);
    }

    private BstNode<E> get(BstNode<E> n, boolean findMax) {
        BstNode<E> parent = null;
        while (n != null) {
            parent = n;
            n = (findMax) ? n.right : n.left;
        }
        return parent;
    }

    /**
     * Grąžinamas aibės elementų masyvas.
     *
     * @return Grąžinamas aibės elementų masyvas.
     */
    @Override
    public Object[] toArray() {
        int i = 0;
        Object[] array = new Object[size];
        for (Object o : this) {
            array[i++] = o;
        }
        return array;
    }

    /**
     * Grąžinamas dvejetainio paieškos medžio šaknis
     *
     * @return Grąžinamas dvejetainio paieškos medžio šaknies mazgas.
     */
    BstNode<E> getRoot() {
        return root;
    }

    /**
     * Keičiama dvejetainio paieškos medžio šaknis
     *
     * @param root šaknies mazgas.
     */
    void setRoot(BstNode<E> root) {
        this.root = root;
    }

    /**
     * Keičiamas aibės elementų kiekis
     *
     * @param size elementų kiekis.
     */
    void setSize(int size) {
        this.size = size;
    }

    /**
     * Aibės elementų išvedimas į String eilutę Inorder (Vidine) tvarka. Aibės
     * elementai išvedami surikiuoti didėjimo tvarka pagal raktą.
     *
     * @return elementų eilutė
     */
    @Override
    public String toString() {
        return toStringRecursive(root);
    }

    String toStringRecursive(BstNode<E> n) {
        if (n == null) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(toStringRecursive(n.left));
        sb.append(n.element.toString()).append(System.lineSeparator());
        sb.append(toStringRecursive(n.right));
        return sb.toString();
    }

    //==============================================================================
/* Papildomas metodas, išvedantis aibės elementus į vieną String eilutę.
     * String eilutė formuojama atliekant elementų postūmį nuo krašto,
     * priklausomai nuo elemento lygio medyje. Galima panaudoti spausdinimui į
     * ekraną ar failą tyrinėjant medžio algoritmų veikimą.
     *
     * @author E. Karčiauskas
     */
//==============================================================================
// Medžio vaizdavimas simboliais, žiūr.: unicode.org/charts/PDF/U2500.pdf
// Tai 4 galimi terminaliniai simboliai medžio šakos gale
    private static final String[] term = {"\u2500", "\u2534", "\u252C", "\u253C"};
    private static final String rightEdge = "\u250C";
    private static final String leftEdge = "\u2514";
    private static final String endEdge1 = "\u25AA";
    private static final String endEdge2 = "\u25CF";
    private static final String vertical = "\u2502  ";
    private String horizontal;
    private String endEdge;

    public String toVisualizedString(String treeDrawType, String dataCodeDelimiter) {
        horizontal = term[0] + term[0];
        endEdge = (treeDrawType.equals("Kvadratas")) ? endEdge1 : endEdge2;
        return toTreeDraw(getRoot(), ">", "", dataCodeDelimiter);
    }

    private String toTreeDraw(BstNode<E> node, String edge, String indent, String dataCodeDelimiter) {
        if (node == null) {
            return "";
        }
        String step = (edge.equals(leftEdge)) ? vertical : "   ";
        StringBuilder sb = new StringBuilder();
        sb.append(toTreeDraw(node.right, rightEdge, indent + step, dataCodeDelimiter));
        int t = (node.right != null) ? 1 : 0;
        t = (node.left != null) ? t + 2 : t;
        sb.append(indent).append(edge).append(horizontal).append(term[t]).append(endEdge).append(
                split(node.element.toString(), dataCodeDelimiter)).append(System.lineSeparator());
        step = (edge.equals(rightEdge)) ? vertical : "   ";
        sb.append(toTreeDraw(node.left, leftEdge, indent + step, dataCodeDelimiter));
        return sb.toString();
    }

    private String split(String s, String dataCodeDelimiter) {
        int k = s.indexOf(dataCodeDelimiter);
        if (k <= 0) {
            return s;
        }
        return s.substring(0, k);
    }

    /**
     * Sukuria ir grąžina aibės kopiją.
     *
     * @return Aibės kopija.
     * @throws java.lang.CloneNotSupportedException
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        BstSetKTU<E> cl = (BstSetKTU<E>) super.clone();
        if (root == null) {
            return cl;
        }
        cl.clear();
        cloneRecursive(cl, root);
        cl.size = this.size;
        return cl;
    }

    private BstNode<E> cloneRecursive(BstSetKTU<E> cl, BstNode<E> n) {
        if (n == null) {
            return null;
        }
        cl.add(n.element);
        cloneRecursive(cl, n.left);
        cloneRecursive(cl, n.right);
        return n;
    }

    /**
     * Grąžinamas aibės poaibis iki elemento element.
     *
     * @param element - Aibės elementas.
     * @return Grąžinamas aibės poaibis iki elemento element.
     */
    @Override
    public SetADT<E> headSet(E element) {
        throw new UnsupportedOperationException("Studentams reikia realizuoti headSet()");
    }

    /**
     * Grąžinamas aibės poaibis nuo elemento element1 iki element2.
     *
     * @param element1 - pradinis aibės poaibio elementas.
     * @param element2 - galinis aibės poaibio elementas.
     * @return Grąžinamas aibės poaibis nuo elemento element1 iki element2.
     */
    @Override
    public SetADT<E> subSet(E element1, E element2) {
        throw new UnsupportedOperationException("Studentams reikia realizuoti subSet()");
    }

    /**
     * Grąžinamas aibės poaibis iki elemento element.
     *
     * @param element - Aibės elementas.
     * @return Grąžinamas aibės poaibis nuo elemento element.
     */
    @Override
    public SetADT<E> tailSet(E element) {
        throw new UnsupportedOperationException("Studentams reikia realizuoti tailSet()");
    }

    /**
     * Grąžinamas tiesioginis iteratorius.
     *
     * @return Grąžinamas tiesioginis iteratorius.
     */
    @Override
    public Iterator<E> iterator() {
        return new IteratorKTU(true);
    }

    /**
     * Grąžinamas atvirkštinis iteratorius.
     *
     * @return Grąžinamas atvirkštinis iteratorius.
     */
    @Override
    public Iterator<E> descendingIterator() {
        return new IteratorKTU(false);
    }

//==============================================================================
// Vidinė objektų kolekcijos iteratoriaus klasė.
// Iteratoriai: didėjantis ir mažėjantis.
// Kolekcija iteruojama kiekviena elementa aplankant viena karta vidine (angl.
// inorder) tvarka. Visi aplankyti elementai saugomi steke. Stekas panaudotas iš
// java.util paketo, bet galima susikurti nuosavą.
//==============================================================================
    class IteratorKTU implements Iterator<E> {

        private Stack<BstNode<E>> stack = new Stack<>();
        //Nurodo iteravimo kolekcija kryptį, true - didėjimo tvarka,
        //false - mažėjimo
        private boolean ascending;
        //Nurodo einamojo medžio elemento tėvą. Reikalingas šalinimui.
        private BstNode<E> parent = root;

        IteratorKTU(boolean ascendingOrder) {
            this.ascending = ascendingOrder;
            this.toStack(root);
        }

        @Override
        public boolean hasNext() {
            return !stack.empty();
        }

        @Override
        public E next() {
            if (!stack.empty()) {
                BstNode<E> n = stack.pop();
                parent = (!stack.empty()) ? stack.peek() : root;
                BstNode node = (ascending) ? n.right : n.left;
                toStack(node);
                return n.element;
            } else {
                return null;
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Studentams reikia realizuoti remove()");
        }

        private void toStack(BstNode<E> n) {
            while (n != null) {
                stack.push(n);
                n = (ascending) ? n.left : n.right;
            }
        }
    }

//==============================================================================
//Vidinė kolekcijos mazgo klasė
//==============================================================================
    class BstNode<E> {

        //Elementas
        E element;
        //Rodyklė į kairįjį pomedį
        BstNode<E> left;
        //Rodyklė į dešinįjį pomedį
        BstNode<E> right;

        BstNode() {
        }

        BstNode(E element) {
            this.element = element;
            this.left = null;
            this.right = null;
        }
    }
}
