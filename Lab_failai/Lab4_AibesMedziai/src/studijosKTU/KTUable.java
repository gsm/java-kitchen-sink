package studijosKTU;

/*
 * Interfeisas, kurį turi tenkinti studentų kuriamos duomenų klasės
 * @author EK
 */
public interface KTUable<T> extends Comparable<T> {

    KTUable<T> create(String dataString); // sukuria naują objektą iš eilutės

    String validate();                // patikrina objekto reikšmes

    void fromString(String e);         // suformuoja  objektą iš eilutės

    @Override
    int compareTo(T e);                 // this objektas sulyginamas su e obj.

    @Override
    String toString();                 // atvaizduoja objektą į eilutę
}
