package LaboraiDemo;

import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;
import studijosKTU.*;

/**
 * @author EK
 */
public final class Automobilis implements KTUable<Automobilis> {

    // bendri duomenys visiems automobiliams (visai klasei)
    static private int priimtinuMetuRiba = 1990;
    static private int dabartiniaiMetai = 2010;
    static private double maziausiaKaina = 100.0;
    static private double didziausiaKaina = 333000.0;
    static private final String idCode = "TA";   //  ***** nauja
    static private int serNr = 100;               //  ***** nauja
    private final String autoRegNr;
    private String marke = "";
    private String modelis = "";
    private int gamMetai = -1;
    private int rida = -1;
    private double kaina = -1.0;

    public Automobilis() {
        autoRegNr = idCode + (serNr++);    // suteikiamas originalus autoRegNr
    }

    public Automobilis(String markė, String modelis,
            int gamMetai, int rida, double kaina) {
        autoRegNr = idCode + (serNr++);    // suteikiamas originalus autoRegNr
        this.marke = markė;
        this.modelis = modelis;
        this.gamMetai = gamMetai;
        this.rida = rida;
        this.kaina = kaina;
        validate();
    }

    public Automobilis(String e) {
        autoRegNr = idCode + (serNr++);    // suteikiamas originalus autoRegNr
        this.fromString(e);
    }

    @Override
    public Automobilis create(String dataString) {
        return new Automobilis(dataString);
    }

    @Override
    public String validate() {
        String klaidosTipas = "";
        if (gamMetai < priimtinuMetuRiba || gamMetai > dabartiniaiMetai) {
            klaidosTipas = "Blogai nurodyti gamybos metai; ";
        }
        if (kaina < maziausiaKaina || kaina > didziausiaKaina) {
            klaidosTipas += "Kaina už leistinų ribų; ";
        }
        return klaidosTipas;
    }

    @Override
    public void fromString(String autoD) {
        try {   // ed - tai elementarūs duomenys, atskirti tarpais
            Scanner ed = new Scanner(autoD);
            marke = ed.next();
            modelis = ed.next();
            gamMetai = ed.nextInt();
            setRida(ed.nextInt());
            setKaina(ed.nextDouble());
            validate();
        } catch (InputMismatchException e) {
        } catch (NoSuchElementException e) {
        }
    }

    @Override
    public String toString() {  // papildyta su autoRegNr
        return getAutoRegNr() + "=" + marke + "_" + modelis + ":" + gamMetai + " " + getRida() + " "
                + String.format("%4.1f", kaina);
    }

    public String getMarkė() {
        return marke;
    }

    public String getModelis() {
        return modelis;
    }

    public int getGamMetai() {
        return gamMetai;
    }

    public int getRida() {
        return rida;
    }

    public double getKaina() {
        return kaina;
    }

    public void setKaina(double kaina) {
        this.kaina = kaina;
    }

    @Override
    public int compareTo(Automobilis a) {
        return getAutoRegNr().compareTo(a.getAutoRegNr());
    }

    public static Comparator<Automobilis> pagalMarke = (Automobilis a1, Automobilis a2) -> a1.marke.compareTo(a2.marke); // pradžioje pagal markes, o po to pagal modelius

    public static Comparator<Automobilis> pagalKaina = (Automobilis a1, Automobilis a2) -> {
        // didėjanti tvarka, pradedant nuo mažiausios
        if (a1.kaina < a2.kaina) {
            return -1;
        }
        if (a1.kaina > a2.kaina) {
            return +1;
        }
        return 0;
    };
    public static Comparator<Automobilis> pagalMetusKainą = (Automobilis a1, Automobilis a2) -> {
        // metai mažėjančia tvarka, esant vienodiems lyginama kaina
        if (a1.gamMetai > a2.gamMetai) {
            return +1;
        }
        if (a1.gamMetai < a2.gamMetai) {
            return -1;
        }
        if (a1.kaina > a2.kaina) {
            return +1;
        }
        if (a1.kaina < a2.kaina) {
            return -1;
        }
        return 0;
    };

    public String getAutoRegNr() {  //** nauja. Reikia maisos lentelems
        return autoRegNr;
    }

    public void setRida(int rida) {
        this.rida = rida;
    }

}
