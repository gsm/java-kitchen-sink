package LaboraiDemo;

import java.util.*;
import studijosKTU.*;

/*
 * Aibės testavimas be Swing'o
 *
 */
public class AutoTestai {

    static Automobilis[] autoBaze1;
    static SortedSetADTx<Automobilis> aSerija = new BstSetKTUx(new Automobilis(), Automobilis.pagalKaina);
    static Random ag = new Random();  // Atsitiktinių generatorius

    public static void main(String[] args) throws CloneNotSupportedException {
        Locale.setDefault(Locale.US); // suvienodiname skaičių formatus
        aibėsTestas();
    }

    static SortedSetADTx generuotiAibe(int kiekis, int generN) {
        String[][] am = { // galimų automobilių markių ir jų modelių masyvas
            {"Mazda", "121", "323", "626", "MX6"},
            {"Ford", "Fiesta", "Escort", "Focus", "Sierra", "Mondeo"},
            {"Saab", "92", "96"},
            {"Honda", "Accord", "Civic", "Jazz"},
            {"Renault", "Laguna", "Megane", "Twingo", "Scenic"},
            {"Peugeot", "206", "207", "307"}
        };
        autoBaze1 = new Automobilis[generN];
        ag.setSeed(1949);
        for (int i = 0; i < generN; i++) {
            int ma = ag.nextInt(am.length);        // markės indeksas  0..
            int mo = ag.nextInt(am[ma].length - 1) + 1;// modelio indeksas 1..
            autoBaze1[i] = new Automobilis(am[ma][0], am[ma][mo],
                    1990 + ag.nextInt(20), // metai tarp 1990 ir 2009
                    6000 + ag.nextInt(222000), // rida tarp 6000 ir 228000
                    800 + ag.nextDouble() * 88000); // kaina tarp 800 ir 88800
        }
        Collections.shuffle(Arrays.asList(autoBaze1));
        aSerija.clear();
        for (int i = 0; i < kiekis; i++) {
            aSerija.add(autoBaze1[i]);
        }
        return aSerija;
    }

    public static void aibėsTestas() throws CloneNotSupportedException {
        Automobilis a1 = new Automobilis("Renault", "Laguna", 1997, 50000, 1700);
        Automobilis a2 = new Automobilis("Renault", "Megane", 2001, 20000, 3500);
        Automobilis a3 = new Automobilis("Toyota", "Corolla", 2001, 20000, 3500);
        Automobilis a4 = new Automobilis("Renault Laguna 2001 115900 700");
        Automobilis a5 = new Automobilis("Renault Megane 1946 365100 9500");
        Automobilis a6 = new Automobilis("Honda   Civic  2001  36400 80.3");
        Automobilis a7 = new Automobilis("Renault Laguna 2001 115900 7500");
        Automobilis a8 = new Automobilis("Renault Megane 1946 365100 950");
        Automobilis a9 = new Automobilis("Honda   Civic  2007  36400 850.3");

        Automobilis[] autoMasyvas = {a9, a7, a8, a5, a1, a6};

        Ks.oun("Auto Aibė:");
        SortedSetADTx<Automobilis> autoAibe = new BstSetKTUx(new Automobilis());
        for (Automobilis a : autoMasyvas) {
            autoAibe.add(a);
        }
        Ks.oun(autoAibe.toVisualizedString("Apskritimas", ""));

        SortedSetADTx<Automobilis> autoAibe2 =
                (SortedSetADTx<Automobilis>) autoAibe.clone();

        autoAibe2.add(a2);
        autoAibe2.add(a3);
        autoAibe2.add(a4);
        Ks.oun("Papildyta autoaibės kopija:");
        Ks.oun(autoAibe2.toVisualizedString("Apskritimas", ""));

        Ks.oun("Originalas:");
        Ks.ounn(autoAibe.toVisualizedString("Kvadratas", ""));

        Ks.oun("Automobilių aibė su iteratoriumi:");
        Ks.oun("");
        for (Automobilis a : autoAibe) {
            Ks.oun(a);
        }
        Ks.oun("");
        Ks.oun("Automobilių aibė AVL-medyje:");
        SortedSetADTx<Automobilis> autoAibe3 = new AvlSetKTUx(new Automobilis());
        for (Automobilis a : autoMasyvas) {
            autoAibe3.add(a);
        }
        Ks.ounn(autoAibe3.toVisualizedString("Kvadratas", ""));

        Ks.oun("Automobilių aibė su iteratoriumi:");
        Ks.oun("");
        for (Automobilis a : autoAibe3) {
            Ks.oun(a);
        }

        Ks.oun("");
        Ks.oun("Automobilių aibė su atvirkštiniu iteratoriumi:");
        Ks.oun("");
        Iterator iter = autoAibe3.descendingIterator();
        while (iter.hasNext()) {
            Ks.oun(iter.next());
        }

        //Išvalome ir suformuojame aibes skaitydami iš failo
        autoAibe.clear();
        autoAibe3.clear();

        Ks.oun("");
        Ks.oun("Automobilių aibė DP-medyje:");
        autoAibe.load("ban.txt");
        Ks.ounn(autoAibe.toVisualizedString("Apskritimas", ""));
        Ks.oun("Išsiaiškinkite, kodėl medis augo tik į vieną pusę.");

        Ks.oun("");
        Ks.oun("Automobilių aibė AVL-medyje:");
        autoAibe3.load("ban.txt");
        Ks.ounn(autoAibe3.toVisualizedString("Apskritimas", ""));

        SetADT<String> autoAibe4 = AutoApskaita.automobiliuMarkes(autoMasyvas);
        Ks.oun("Pasikartojančios automobilių markės:\n" + autoAibe4.toString());
    }
}
