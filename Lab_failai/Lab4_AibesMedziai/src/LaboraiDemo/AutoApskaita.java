package LaboraiDemo;

import studijosKTU.*;

public class AutoApskaita {

    public static SetADT<String> automobiliuMarkes(Automobilis[] auto) {
        SetADT<Automobilis> uni = new BstSetKTU<>(Automobilis.pagalMarke);
        SetADT<String> kart = new BstSetKTU<>();
        for (Automobilis a : auto) {
            if (!uni.add(a)) {
                kart.add(a.getMarkė());
            }
        }
        return kart;
    }
}
