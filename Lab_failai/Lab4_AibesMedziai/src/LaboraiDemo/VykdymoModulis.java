package LaboraiDemo;

import GUI.*;
import java.util.Locale;
/*
 * Darbo Atlikimo tvarka - čia yra pradinė klasė:
 */

public class VykdymoModulis {

    public static void main(String[] args) throws CloneNotSupportedException {
        Locale.setDefault(Locale.US); // suvienodiname skaičių formatus
        AutoTestai.aibėsTestas();
        MainWindow.createAndShowGUI();
    }
}