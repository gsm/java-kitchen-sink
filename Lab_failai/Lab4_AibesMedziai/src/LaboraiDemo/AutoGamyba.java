package LaboraiDemo;

import GUI.KsSwing.MyException;
import java.util.*;

public class AutoGamyba {

    private static final Random ag = new Random();
    private static Automobilis[] masyvas;
    private static int pradinisBaze = 0, galinisBaze = 0;
    private static boolean pradzia = true;

    public static Automobilis[] generuotiAutomobiliuMasyva(int kiekis) {
        //Atsitiktinių generatorius
        String[][] am = { //Galimų automobilių markių ir jų modelių masyvas
            {"Mazda", "121", "323", "626", "MX3", "MX6", "3", "6"},
            {"Ford", "Fiesta", "Escort", "Focus", "Sierra", "Mondeo"},
            {"Saab", "92", "96"},
            {"Honda", "Accord", "Civic", "Jazz", "Legend"},
            {"Renault", "Laguna", "Megane", "Twingo", "Scenic"},
            {"Peugeot", "206", "207", "307"},
            {"Wolkswagen", "Polo", "Golf", "Vento", "Passat", "Tuareg",
                "Transporter", "Touran", "Sharan"},
            {"Audi", "80", "100", "200", "A3", "A4", "A6", "A8", "Q1"}
        };

        masyvas = new Automobilis[kiekis];
        ag.setSeed(1949);
        for (int i = 0; i < kiekis; i++) {
            int ma = ag.nextInt(am.length);        // markės indeksas  0..
            int mo = ag.nextInt(am[ma].length - 1) + 1;// modelio indeksas 1..
            masyvas[i] = new Automobilis(am[ma][0], am[ma][mo],
                    1990 + ag.nextInt(20), // metai tarp 1990 ir 2009
                    6000 + ag.nextInt(222000), // rida tarp 6000 ir 228000
                    800 + ag.nextDouble() * 88000); // kaina tarp 800 ir 88800
        }
        return masyvas;
    }

    public static Automobilis[] generuotiIrIsmaisyti(int aibe, int aibesImtis,
            double isbarstKoef) throws MyException {
        masyvas = generuotiAutomobiliuMasyva(aibe);
        return ismaisyti(masyvas, aibesImtis, isbarstKoef);
    }

    //Galima paduoti masyvą išmaišymui iš išorės
    public static Automobilis[] ismaisyti(Automobilis[] autoBaze,
            int kiekis, double maisymoDalis) throws MyException {
        if (autoBaze == null) {
            throw new NullPointerException("Null pointeris ismaisyti() metode");
        }
        if (kiekis <= 0) {
            throw new MyException(kiekis + "", 0);
        }
        if (autoBaze.length < kiekis * 3) {
            throw new MyException(autoBaze.length + " > " + kiekis + "*3", 3);
        }
        if ((maisymoDalis < 0) || (maisymoDalis > 1)) {
            throw new MyException(maisymoDalis + "", 2);
        }
        Automobilis temp;
        //Kad medis augtų ne į vieną pusę, pradinė generuojama aibė paimama
        //ne iš masyvo pradžios, o iš atsitiktinės vietos, ir perrašoma į 
        //pradžią sukeičiant elementus
        int index = 0;
        if (autoBaze.length - (kiekis * 2) > 0) {
            index = ag.nextInt(autoBaze.length - (kiekis * 2));
        }
        index += kiekis;
        for (int i = 0; i < kiekis; i++) {
            temp = autoBaze[i];
            autoBaze[i] = autoBaze[index];
            autoBaze[index] = temp;
            index++;
        }
        //Likusi masyvo dalis surūšiuojama
        Arrays.sort(autoBaze, kiekis, autoBaze.length);
        //Išmaišoma spausdinama aibės imtis
        int j1 = (int) (kiekis * maisymoDalis / 2.0) + 1;
        int j2 = (kiekis - j1) < 0 ? 0 : kiekis - j1;
        if (j1 > 1) {
            Collections.shuffle(Arrays.asList(autoBaze).subList(0, j1));
        }
        if (j2 < (kiekis - 1)) {
            Collections.shuffle(Arrays.asList(autoBaze).subList(j2, kiekis));
        }
        //Išmaišoma likusi aibės imtis
        j1 = (int) ((autoBaze.length + kiekis) * maisymoDalis / 2.0) + 1;
        j2 = (autoBaze.length + kiekis - j1) < 0 ? 0 : autoBaze.length + kiekis - j1;
        if (j1 > kiekis + 1) {
            Collections.shuffle(Arrays.asList(autoBaze).subList(kiekis + 1, j1));
        }
        if (j2 < (autoBaze.length - 1)) {
            Collections.shuffle(Arrays.asList(autoBaze).subList(j2, autoBaze.length));
        }
        pradinisBaze = kiekis;
        galinisBaze = autoBaze.length - 1;
        AutoGamyba.masyvas = autoBaze;
        return Arrays.copyOf(masyvas, kiekis);
    }

    public static Automobilis imtiIsBazes() throws MyException {
        if ((galinisBaze - pradinisBaze) < 0) {
            throw new MyException(galinisBaze - pradinisBaze + "", 4);
        }
        //Vieną kartą automobilis imamas iš masyvo pradzios, kitą kartą - iš galo.
        if (pradzia) {
            pradzia = false;
            return masyvas[pradinisBaze++];
        } else {
            pradzia = true;
            return masyvas[galinisBaze--];
        }
    }
}
