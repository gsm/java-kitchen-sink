package GUI;

import java.lang.reflect.InvocationTargetException;
import java.util.ResourceBundle;
import javax.swing.*;
import studijosKTU.Ks;

/**
 * @author matas
 */
public class Applet extends JApplet {

    private final ResourceBundle rb = ResourceBundle.getBundle("GUI.MyResources");

    @Override
    public void init() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            SwingUtilities.invokeAndWait(() -> {
                add(new Lab4Panel(rb));
            });
        } catch (InterruptedException | InvocationTargetException | ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            Ks.ou(ex.getMessage());
        }
    }
}
