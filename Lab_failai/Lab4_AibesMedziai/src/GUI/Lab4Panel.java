package GUI;

import GUI.KsSwing.MyException;
import LaboraiDemo.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.ResourceBundle;
import javax.swing.*;
import javax.swing.border.TitledBorder;
import studijosKTU.*;

/**
 * @author darius.matulis@ktu.lt
 */
//Lab4 panelis
public class Lab4Panel extends JPanel implements ActionListener {

    private final ResourceBundle rb;

//                              Lab4Panel (BorderLayout)
//  |---------------------------------Center--------------------------------|
//  |  |~~~~~~~~~~~~~~~~~~~~~~~~~~~~splResults~~~~~~~~~~~~~~~~~~~~~~~~~~~|  |
//  |  |  |-----------------------------------|  |--------------------|  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |            scrollTree             |  |    scrollOutput    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |                                   |  |                    |  |  |
//  |  |  |-----------------------------------|  |--------------------|  |  |
//  |  |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|  |                                                  | 
//  |---------------------------------South---------------------------------|
//  |  |~~~~~~~~~~~~~~~~~~~~~~~~~~~~scrollSouth~~~~~~~~~~~~~~~~~~~~~~~~~~|  |
//  |  |                                                                 |  |
//  |  |             |-----------||-----------||------------|            |  | 
//  |  |             | panParam1 || panParam2 || panButtons |            |  |
//  |  |             |           ||           ||            |            |  |
//  |  |             |-----------||-----------||------------|            |  |
//  |  |                                                                 |  |
//  |  |~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~|  |
//  |-----------------------------------------------------------------------| 
    public Lab4Panel(ResourceBundle rb) {
        this.rb = rb;
        initComponents();
    }

    private void initComponents() {
        //======================================================================
        // Sudaromas rezultatų išvedimo JSplitPane klasės objektas, kuriame
        // talpinami du JTextArea klasės objektai
        //======================================================================
        splResults.setLeftComponent(scrollTree);
        splResults.setRightComponent(scrollOutput);
        splResults.setDividerLocation(570);
        splResults.setResizeWeight(0.7);
        splResults.setDividerSize(5);
        splResults.setPreferredSize(new Dimension(1000, 400));
        //Kad prijungiant tekstą prie JTextArea vaizdas visada nušoktų į apačią
        scrollTree.getVerticalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
            taTree.select(taTree.getCaretPosition()
                    * taTree.getFont().getSize(), 0);
        });
        scrollOutput.getVerticalScrollBar().addAdjustmentListener((AdjustmentEvent e) -> {
            taOutput.select(taOutput.getCaretPosition()
                    * taOutput.getFont().getSize(), 0);
        });
        //======================================================================
        // Formuojama pirmoji parametrų lentelė (gelsva).
        //======================================================================
        panParam1.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(3, 6, 3, 4);
        //Išlygiavimas į kairę
        c.anchor = GridBagConstraints.WEST;
        //Komponentų išplėtimas iki maksimalaus celės dydžio
        c.fill = GridBagConstraints.BOTH;
        //Pirmas stulpelis
        c.gridx = 0;
        for (String s : rb.getStringArray("lblParams1")) {
            panParam1.add(new JLabel(s), c);
        }
        //Antras stulpelis
        c.gridx = 1;
        for (String s : rb.getStringArray("cmbTreeTypes")) {
            cmbTreeType.addItem(s);
        }
        cmbTreeType.addActionListener(this);
        panParam1.add(cmbTreeType, c);
        for (String s : rb.getStringArray("cmbTreeSymbols")) {
            cmbTreeSymbols.addItem(s);
        }
        panParam1.add(cmbTreeSymbols, c);
        tfDelimiter.setHorizontalAlignment(JTextField.CENTER);
        panParam1.add(tfDelimiter, c);
        //Vėl pirmas stulpelis, tačiau plotis - 2 celės
        c.gridx = 0;
        c.gridwidth = 2;
        tfInput.setEditable(false);
        tfInput.setBackground(Color.lightGray);
        panParam1.add(tfInput, c);
        //======================================================================
        // Formuojama antroji parametrų lentelė (oranžinė). Naudojame klasę MyPanels.
        //======================================================================
        //8 - JTextField'ų plotis
        panParam2 = new MyPanels(rb.getStringArray("lblParams2"),
                rb.getStringArray("tfParams2"), 8);
        //======================================================================
        // Formuojamas mygtukų tinklelis (mėlynas). Naudojame klasę MyPanels.
        //======================================================================
        //4 eilutes, stulpeliu - neribotai
        panButtons = new MyPanels(rb.getStringArray("btnLabels"), 4, 0);
        for (JButton btn : panButtons.getButtons()) {
            btn.addActionListener(this);
        }
        enableButtons(false);
        //======================================================================
        // Formuojamas bendras parametrų panelis (tamsiai pilkas).
        //======================================================================
        panSouth.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
        panSouth.add(panParam1);
        panSouth.add(panParam2);
        panSouth.add(panButtons);
        //======================================================================
        // Formuojamas Lab4 panelis
        //======================================================================        
        setLayout(new BorderLayout());
        //..centre ir pietuose talpiname objektus..
        add(splResults, BorderLayout.CENTER);
        add(scrollSouth, BorderLayout.SOUTH);
        appearance();
    }

    private void appearance() {
        //Grafinių objektų rėmeliai
        int counter = 0;
        for (JComponent comp : new JComponent[]{scrollTree, scrollOutput,
            scrollSouth}) {
            TitledBorder tb = new TitledBorder(rb.getStringArray("lblBorders")[counter++]);
            tb.setTitleFont(new Font(Font.SANS_SERIF, Font.BOLD, 11));
            comp.setBorder(tb);
        }
        panParam1.setBackground(new Color(255, 255, 153));//Gelsva
        panParam2.setBackground(Color.ORANGE);
        panParam2.getTfOfTable()[2].setEditable(false);
        panParam2.getTfOfTable()[2].setForeground(Color.red);
        panParam2.getTfOfTable()[4].setEditable(false);
        panParam2.getTfOfTable()[4].setBackground(Color.lightGray);
        panButtons.setBackground(new Color(112, 162, 255)); //Blyškiai mėlyna
        panSouth.setBackground(Color.GRAY);
        taTree.setFont(Font.decode("courier new-12"));
        // Swing'as Linux Ubuntu Su Gnome rodo teisingai su šiuo fontu:
        // taTree.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));
        taOutput.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 11));
        taTree.setEditable(false);
        taOutput.setEditable(false);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        try {
            System.gc();
            System.gc();
            System.gc();
            taOutput.setBackground(Color.white);
            taTree.setBackground(Color.white);
            Object source = ae.getSource();
            if (source.equals(panButtons.getButtons()[0])) {
                treeGeneration(null);
            }
            if (source.equals(panButtons.getButtons()[1])) {
                treeIteration();
            }
            if (source.equals(panButtons.getButtons()[2])) {
                treeAdd();
            }
            if (source.equals(panButtons.getButtons()[3])) {
                treeEfficiency();
            }
            if (source.equals(panButtons.getButtons()[4])) {
                treeRemove();
            }
            if (source.equals(panButtons.getButtons()[5])
                    || source.equals(panButtons.getButtons()[6])) {
                KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[0]);
            }
            if (source.equals(cmbTreeType)) {
                enableButtons(false);
            }
        } catch (MyException e) {
            if (e.getCode() >= 0 && e.getCode() <= 3) {
                KsSwing.ounerr(taOutput, rb.getStringArray("errMsgs2")[e.getCode()] + ": " + e.getMessage());
                if (e.getCode() == 3) {
                    panParam2.getTfOfTable()[0].setBackground(Color.red);
                    panParam2.getTfOfTable()[1].setBackground(Color.red);
                }
            } else if (e.getCode() == 4) {
                KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[2]);
            } else {
                KsSwing.ounerr(taOutput, e.getMessage());
            }
        } catch (NullPointerException e) {
            KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[11]);
            e.printStackTrace(System.out);
        } catch (IllegalArgumentException e) {
            KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[11]);
            e.printStackTrace(System.out);
        } catch (UnsupportedOperationException e) {
            KsSwing.ounerr(taOutput, e.getMessage());
        } catch (Exception e) {
            KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[11]);
            e.printStackTrace(System.out);
        }
    }

    void treeGeneration(String fName) throws MyException {
        //Nuskaitomi uždavinio parametrai
        getParameters();
        //Sukuriamas aibės objektas, priklausomai nuo medžio pasirinkimo
        //cmbTreeType objekte
        switch (cmbTreeType.getSelectedIndex()) {
            case 0:
                autoSet = new BstSetKTUx(new Automobilis());
                break;
            case 1:
                autoSet = new AvlSetKTUx(new Automobilis());
                break;
            default:
                enableButtons(false);
                throw new MyException(rb.getStringArray("msgs")[0]);
        }
        //Jei failas nenaudojamas
        if (fName == null) {
            autoArray = AutoGamyba.generuotiIrIsmaisyti(sizeOfGenSet, sizeOfInitialSubSet, coef);
            panParam2.getTfOfTable()[2].setText(sizeOfLeftSubSet + "");
        } else { //Jei skaitoma is failo
            autoSet.load(fName);
            autoArray = new Automobilis[autoSet.size()];
            int i = 0;
            for (Object o : autoSet.toArray()) {
                autoArray[i++] = (Automobilis) o;
            }
            //Skaitant iš failo išmaišoma standartiniu Collections.shuffle metodu.
            Collections.shuffle(Arrays.asList(autoArray), new Random());
        } //Išmaišyto masyvo elementai surašomi i aibę
        autoSet.clear();
        for (Automobilis a : autoArray) {
            autoSet.add(a);
        }
        //Išvedami rezultatai
        //Nustatoma, kad eilutės pradžioje neskaičiuotų išvedamų eilučių skaičiaus
        KsSwing.setFormatStartOfLine(false);
        KsSwing.oun(taTree, autoSet.toVisualizedString(treeDrawType, delimiter),
                rb.getStringArray("msgs")[4]);
        //Nustatoma, kad eilutės pradžioje skaičiuotų išvedamų eilučių skaičių
        KsSwing.setFormatStartOfLine(true);
        KsSwing.oun(taOutput, autoSet, rb.getStringArray("msgs")[5]);
        enableButtons(true);
    }

    private void treeAdd() throws MyException {
        Automobilis auto = AutoGamyba.imtiIsBazes();
        autoSet.add(auto);
        panParam2.getTfOfTable()[2].setText(--sizeOfLeftSubSet + "");
        KsSwing.setFormatStartOfLine(false);
        KsSwing.oun(taTree, auto, rb.getStringArray("msgs")[6]);
        KsSwing.oun(taTree, autoSet.toVisualizedString(treeDrawType, delimiter));
        KsSwing.setFormatStartOfLine(true);
        KsSwing.oun(taOutput, autoSet, rb.getStringArray("msgs")[6]);
    }

    private void treeRemove() {
        if (autoSet.isEmpty()) {
            KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[10]);
        } else {
            int nr = new Random().nextInt(autoSet.size());
            Automobilis auto = (Automobilis) autoSet.toArray()[nr];
            autoSet.remove(auto);
            KsSwing.setFormatStartOfLine(false);
            KsSwing.oun(taTree, auto, rb.getStringArray("msgs")[8]);
            KsSwing.oun(taTree, autoSet.toVisualizedString(treeDrawType, delimiter));
            KsSwing.setFormatStartOfLine(true);
            if (autoSet.isEmpty()) {
                KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[10]);
            } else {
                KsSwing.oun(taOutput, autoSet, rb.getStringArray("msgs")[8]);
            }
        }
    }

    private void treeIteration() {
        if (autoSet.isEmpty()) {
            KsSwing.ounerr(taOutput, rb.getStringArray("msgs")[10]);
        } else {
            KsSwing.oun(taOutput, autoSet, rb.getStringArray("msgs")[9]);
        }
    }

    private void treeEfficiency() throws MyException {
        KsSwing.oun(taOutput, "", rb.getStringArray("msgs")[1]);
        SwingUtilities.invokeLater(() -> {
            GreitaveikosTyrimas gt = new GreitaveikosTyrimas(taOutput, panButtons.getButtons(), rb);
            gt.start();
        });
    }

    private void getParameters() throws MyException {
        //Truputėlis kosmetikos..
        for (int i = 0; i < 3; i++) {
            panParam2.getTfOfTable()[i].setBackground(Color.WHITE);
        }
        //Nuskaitomos parametrų reiksmės. Jei konvertuojant is String
        //įvyksta klaida, sugeneruojama NumberFormatException situacija. Tam, kad
        //atskirti kuriame JTextfield'e ivyko klaida, panaudojama nuosava
        //situacija MyException
        int i = 0;
        try {
            //Pakeitimas (replace) tam, kad sukelti situaciją esant
            //neigiamam skaičiui            
            sizeOfGenSet = Integer.parseInt(panParam2.getParametersOfTable()[i].replace("-", "x"));
            sizeOfInitialSubSet = Integer.parseInt(panParam2.getParametersOfTable()[++i].replace("-", "x"));
            sizeOfLeftSubSet = sizeOfGenSet - sizeOfInitialSubSet;
            ++i;
            coef = Double.parseDouble(panParam2.getParametersOfTable()[++i].replace("-", "x"));
        } catch (NumberFormatException e) {
            //Galima ir taip: pagauti ir vėl mesti
            throw new MyException(panParam2.getParametersOfTable()[i], i);
        }
        //Nuskaitomas medžio elemento simbolis ir celės teksto kirtiklis
        treeDrawType = (String) cmbTreeSymbols.getSelectedItem();
        delimiter = tfDelimiter.getText();
    }

    private void enableButtons(boolean enable) {
        for (int i : new int[]{1, 2, 4, 5, 6}) {
            panButtons.getButtons()[i].setEnabled(enable);
        }
    }

    public JTextArea getTaOutput() {
        return taOutput;
    }

    private final JTextArea taTree = new JTextArea();
    private final JScrollPane scrollTree = new JScrollPane(taTree);
    private final JTextArea taOutput = new JTextArea();
    private final JScrollPane scrollOutput = new JScrollPane(taOutput);
    private final JSplitPane splResults = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
    private final JTextField tfDelimiter = new JTextField("", 8);
    private final JTextField tfInput = new JTextField(15);
    private final JComboBox cmbTreeSymbols = new JComboBox();
    private final JComboBox cmbTreeType = new JComboBox();
    private final JPanel panSouth = new JPanel();
    private final JScrollPane scrollSouth = new JScrollPane(panSouth);
    private final JPanel panParam1 = new JPanel();
    private MyPanels panParam2, panButtons;
    private static Automobilis[] autoArray;
    private static SortedSetADTx<Automobilis> autoSet;
    private int sizeOfInitialSubSet, sizeOfGenSet, sizeOfLeftSubSet;
    private double coef;
    private String treeDrawType, delimiter;
}
