/** @author Eimutis Karčiauskas, KTU IF Programų inžinerijos katedra, 2013 09 02
 *
 * Tai yra demonstracinė sumuoklio klasė, kuris prideda prekės kainą prie
 *        kaupiamos sumos.
 * Ji turi sumavimo metodus vienai prekei ir kelioms prekėms.
   *  IŠBANDYKITE pilnai objektinę Java programą.
   *  IŠBANDYKITE veikimą su reset metodu.
   ****************************************************************************/

public class D_Sumuoklis {
    static int sumuokliųKiekis;
    int kasosNr;
    double kaupiamaSuma;

    public D_Sumuoklis() {
        kasosNr = sumuokliųKiekis++;
    }
    double sumuoti(double kaina){
        kaupiamaSuma += kaina;
        System.out.println("Kasa nr." + kasosNr + ": " + kaupiamaSuma);
        return kaupiamaSuma;
    }
    double sumuoti(int kiekis, double kaina){
        kaupiamaSuma += kiekis * kaina;
        System.out.println("Kasa nr." + kasosNr + ": " + kaupiamaSuma);
        return kaupiamaSuma;
    }
    double reset() { 
        kaupiamaSuma = 0;
        return kaupiamaSuma; 
    }
    
    public static void main(String[] args) {
        D_Sumuoklis s0 = new D_Sumuoklis();
        D_Sumuoklis s1 = new D_Sumuoklis();
        D_Sumuoklis s2 = new D_Sumuoklis();
        s0.sumuoti(15.68);
        s1.sumuoti(2, 0.91);
        s2.sumuoti(35.68);
        s0.sumuoti(20.00);
        s2.sumuoti(40.00);
        s0.sumuoti(5, 40.0);
        s1.sumuoti(6, 50.0);
        s2.sumuoti(100_000, 0.03);
    }
}
