package edu.ktu.p175b014.gsm.minsmo;

import java.util.logging.Level;

/**
 *
 * @author Mindaugas Smolinskas, mindaugas.smolinskas@ktu.lt
 */
public class Logger {
    

    public static Logger getLogger(Class<?> type) {
        return new Logger(com.sun.istack.internal.logging.Logger.getLogger(type));
    }

    public static Logger getLogger(String string, Class<?> type) {
        return new Logger(com.sun.istack.internal.logging.Logger.getLogger(string, type));
    }

    private final com.sun.istack.internal.logging.Logger delegate;    
    
    public Logger(com.sun.istack.internal.logging.Logger delegate) {
        this.delegate = delegate;
    }

    public void log(Level level, String string) {
        delegate.log(level, string);
    }

    public void log(Level level, String string, Object o) {
        delegate.log(level, string, o);
    }

    public void log(Level level, String string, Object[] os) {
        delegate.log(level, string, os);
    }

    public void log(Level level, String string, Throwable thrwbl) {
        delegate.log(level, string, thrwbl);
    }

    public void finest(String string) {
        delegate.finest(string);
    }

    public void finest(String string, Object[] os) {
        delegate.finest(string, os);
    }

    public void finest(String string, Throwable thrwbl) {
        delegate.finest(string, thrwbl);
    }

    public void finer(String string) {
        delegate.finer(string);
    }

    public void finer(String string, Object[] os) {
        delegate.finer(string, os);
    }

    public void finer(String string, Throwable thrwbl) {
        delegate.finer(string, thrwbl);
    }

    public void fine(String string) {
        delegate.fine(string);
    }

    public void fine(String string, Throwable thrwbl) {
        delegate.fine(string, thrwbl);
    }

    public void info(String string) {
        delegate.info(string);
    }

    public void info(String string, Object[] os) {
        delegate.info(string, os);
    }

    public void info(String string, Throwable thrwbl) {
        delegate.info(string, thrwbl);
    }

    public void config(String string) {
        delegate.config(string);
    }

    public void config(String string, Object[] os) {
        delegate.config(string, os);
    }

    public void config(String string, Throwable thrwbl) {
        delegate.config(string, thrwbl);
    }

    public void warning(String string) {
        delegate.warning(string);
    }

    public void warning(String string, Object[] os) {
        delegate.warning(string, os);
    }

    public void warning(String string, Throwable thrwbl) {
        delegate.warning(string, thrwbl);
    }

    public void severe(String string) {
        delegate.severe(string);
    }

    public void severe(String string, Object[] os) {
        delegate.severe(string, os);
    }

    public void severe(String string, Throwable thrwbl) {
        delegate.severe(string, thrwbl);
    }

    public boolean isMethodCallLoggable() {
        return delegate.isMethodCallLoggable();
    }

    public boolean isLoggable(Level level) {
        return delegate.isLoggable(level);
    }

    public void setLevel(Level level) {
        delegate.setLevel(level);
    }

    public void entering() {
        delegate.entering();
    }

    public void entering(Object... os) {
        delegate.entering(os);
    }

    public void exiting() {
        delegate.exiting();
    }

    public void exiting(Object o) {
        delegate.exiting(o);
    }

    public <T extends Throwable> T logSevereException(T t, Throwable thrwbl) {
        return delegate.<T>logSevereException(t, thrwbl);
    }

    public <T extends Throwable> T logSevereException(T t, boolean bln) {
        return delegate.<T>logSevereException(t, bln);
    }

    public <T extends Throwable> T logSevereException(T t) {
        return delegate.<T>logSevereException(t);
    }

    public <T extends Throwable> T logException(T t, Throwable thrwbl, Level level) {
        return delegate.<T>logException(t, thrwbl, level);
    }

    public <T extends Throwable> T logException(T t, boolean bln, Level level) {
        return delegate.<T>logException(t, bln, level);
    }

    public <T extends Throwable> T logException(T t, Level level) {
        return delegate.<T>logException(t, level);
    }

    /**
     * Delegates to {@code delegate.equals} with a possible subtle
     * quirk due to encapsulation.
     * 
     * The wrapped object may have the overriden {@code .equals} JITted
     * under some JVMs everywhere, except in this code if it is loaded dynamically.
     * 
     * Under conformant JVMs it just delegates to the unwrapped Logger.equals(o).
     *
      // * @ see delegate#equals
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        Object target = o;
        if (o instanceof Logger) {
            // this is OK; the JVM must note that it can't totally drop
            // the private final field by inlining it (JVMs formally 
            // <em>must</em> manage the field anyway even this might lead to 
            // overheads and inconsistencies in their implementation of the
            // Java Memory Model); the warning is just a counter-measure
            // against coding bugs for too lax JVM implementations.
            //
            // Spec. says that private fields of two instances of the same class
            // are visible to each other. No mention of "final" there!
            target = ((Logger) o).delegate;
        }
        return this.delegate.equals(target);
    }

    @Override
    public int hashCode() {
        return this.delegate.hashCode();
    }

}
