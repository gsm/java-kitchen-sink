package edu.ktu.p175b014.gsm.kindergarten;

import edu.ktu.p175b014.gsm.minsmo.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;

/**
 *
 * @author Administrator
 */
public class Lab1Uzduoteles {

    /**
     * Privatus laukas {@code log} atrodo kaip konstanta, ir noriu, jog JVM jį optimizuotų
     * lyg konstantą vykdymo metu; tačiau realiai tai dinamiškai pagal aplinkybes
     * apskaičiuota vidinė reikšmė: griežtas IDE privalo generuoti įspėjimą, nors
     * tai atlikta specialiai.
     *
     * Rašydamas šią eilutę, griežtai negaliu nustatyti:<ul>
     * <li> kuri klasės Logger realizacija bus užkrauta (galbūt ji bus pakeista),
     * <li> kaip tiksliai atrodys pilnas šios klasės pavadinimas,
     * <li> BET JVM, klasės užkrovimo metu, šiuos faktus žinos.
     * </ul>
     * 
     * Todėl logine prasme, tai NĖRA konstanta.
     * 
     * Ir pavadinu šį globalų kintamąjį NE kaip konstantą, kad vizualiai atrodytų
     * kaip eilinis laukas (plg: {@code System.out}). 
     */
    private static final edu.ktu.p175b014.gsm.minsmo.Logger log = Logger.getLogger(Lab1Uzduoteles.class);

    /**
     * <em>Palindromas</em> – simbolių, žodžių ar pan. seka, kuri skaitoma
     * nuo galo sutampa su pradine.
     * 
     * Šiuo atveju tai <em>neneigiamas</em> sveikasis skaičius, kurio <em>dešimtainė</em>
     * išraiška, skaitoma iš priešingos pusės, turi tą pačią skaitinę reikšmę.
     * 
     * Optimalus sprendinys yra <em>ne</em>konvertuoti skaičiaus į simbolius,
     * o pasinaudoti jo aritmetinėmis savybėmis.
     * 
     * @param skaičius sveikasis skaičius iš intervalo [Long.MIN_VALUE..Long.MAX_VALUE]
     * @return ar duotojo sveikojo skaičiaus dešimtainė išraiška yra palindromas;
     * 0 laikomas palindromu; neigiami skaičiai ar teigiami skaičiai, besibaigiantys
     * nuliu, nėra palindromai
     */
    public static boolean yraPalindromas(long skaičius) {
        // Tikrinam rėžius:
        if(skaičius == 0){ return true; }               // 0 yra palindromas
        else if(skaičius < 0){ return false; }          // neigiami nėra palindromai
        else if(skaičius % 10 == 0){ return false; }    // baigiasi 0, ne palindromas
        
        // konstruojam atvirkščią skaičių ir palyginam su pradiniu:
        long atvirkščias = 0, temp = skaičius;
        while(temp > 0){
            atvirkščias *= 10L;         // pastumiam per poziciją (būtinai Long operacija!)
            atvirkščias += temp % 10L;  // nukopijuojam skaitmenį
            temp /= 10L;                // atmetam skaitmenį
        }
        
        return skaičius == atvirkščias;
    }
    
    
    public static void palindromųDemo() {
        metodoAntraštė();
                
        for(long sk: new long[]{
            -1,             // neigiamas: ne
            0000,           // 0: taip
            0xFF,           // šešioliktainis 255: ne
            0xFC,           // 252: taip
            'B',            // ASCII simbolis 66: taip
            'A',            // 65: ne
            'Š'+1,          // (UTF simbolis 352) + 1: taip
            -((byte)0xFF),  // perpildymas: -(-1): taip
            2+(byte)0xFF,   // perpildymas: 2+(-1): taip            
            1220,           // dalijasi iš 10: ne
            01220,          // aštuonetaine forma 656: taip
            1___0_1,        // pabraukimo simboliai ignoruojami, 101: taip
            '\041',         // šešioliktainis simbolis 33: taip
            \u0030\u0031\u0033,                 // konstanta 013 = aštuonetainis 13 = 11: taip
            '\r'+(2*'\n'),                      // (char)13 + 2*(char)10 = 33: taip
            1234567890987654321L,               // taip
            8642_0000_0000_2468L,               // dešimtainis skaičius, pabraukimo simboliai ignoruojami: taip
            0x8642_0000_0000_2468L,             // šešioliktainis neigiamas skaičius -8772449124164295576: ne
            (int)0x1_0000_000CL + 0xFFFF_FFFF,  // perpildymas (int)12+int(-1)=0: taip
            0x1_0000_000CL + 0xFFFF_FFFF,       // nėra perpildymo: ne
            (short)'\uFFFF' + (short)'\u000C',  // -1 + 12: taip
            
            // konversija int->long yra perteklinė, IDE turi generuoti įspėjimus
            (long)'\uFFFF' + (long)'\u000C',    // 65535 + 12 = 65547: ne - apgaulinga sintaksė, žr. kitą
            (long)(short)'\uFFFF' + (long)'\u000C',    // -1 + 12 = 11: taip - 
        }) {
            System.out.printf("yraPalindromas(%d) \t=> \t%b%n", sk, yraPalindromas(sk) );
        }
    
    }
    
    /**
     * <em>Anagramos</em> yra frazės, kurias galima gauti vieną iš kitos
     * sukeičiant jas sudarančius elementus vietomis.
     * 
     * Šis metodas nustato, ar duotos frazės sudarytos iš vienodų simbolių.
     * Didžiosios ir mažosios raidės laikomos vienodomis. Frazes gali sudaryti:<br>
     * - Angliškos raidės 'A'-'Z', 'a'-'z' (ASCII kodai 65-90, 97-122);<br>
     * - Lietuviškos raidės 'Ą','ą',...'Ž','ž';<br>
     * - Kitų abėcėlių simboliai (Unicode kodai 0x0100 – 0x7FFF).
     * @param žodis1
     * @param žodis2
     * @return Ar duotos frazės yra anagramos.
     */
    public static boolean yraAnagramos(String žodis1, String žodis2){
        Locale locale = new Locale("lt","LT");
        char[] simboliai1 = žodis1.toUpperCase(locale).toCharArray();
        char[] simboliai2 = žodis2.toUpperCase(locale).toCharArray();
        
        Arrays.sort(simboliai1);
        Arrays.sort(simboliai2);
        
        return Arrays.equals(simboliai1, simboliai2);
    }

    public static void anagramųDemo(){
        metodoAntraštė();
        
        for(String[] s:
            new String[][]{
                new String[]{ "Ąžuolas", "ąžuolas" },
                new String[]{ "Kirpti", "Pirkti" },
                new String[]{ "kristi", "skristi" },
                new String[]{ "Režimas", "Rėžimas" },
                new String[]{ "Gámma", "Mágma" },
                new String[]{ "Γάμμα", "Μάγμα" },
                new String[]{ "Желать", "Лежать" },
                new String[]{ "Ave Maria, gratia plena, Dominus tecum",
                    "Virgo serena, pia, munda et immaculata" },             
            }
        ) {
            System.out.printf(
                    "%s\t<-> %s\t=>%s anagramos%n", s[0], s[1],
                    yraAnagramos(s[0], s[1])? "":" ne"
            );
        }
            
    }
    
    /**
     * Atspausdina, kiek kartų pasikartoja žodžiai ar frazės eilučių masyve.
     * Didžiosios ir mažosios ASCII bei Unicode {@code 0x0100-0x1FFF} raidės laikomos identiškomis.
     * Kiti simboliai (skirtukai, skaitmenys, tarpai) taip pat laikomi reikšminiais.
     * 
     * {@code null} atveju laikoma, kad duotas tuščias masyvas.
     * 
     * Šioje realizacijoje naudojama OS lokalė, o Unicode simboliai nenormalizuojami
     * [žr. komentarus kode].
     * 
     * Todėl, priklausomai nuo OS nustatymų, simboliai kaip:
     * <ul>
     * <li>'ą̃',</li>
     * <li>'a' + ' ̨' + '~',</li>
     * <li>'a' + '~' + ' ̨'.</li>
     * </ul>
     * daugumoje lokalių bus laikomi skirtingais.
     * 
     * Rezultatai spausdinami į {@code System.out}.
     * 
     * @param žodžiai Unicode frazių masyvas arba {@code null}
     */
    public static void žodžiųPasikartojimai(String žodžiai[]) {
        // Masyvas tuščias?
        if(žodžiai == null || žodžiai.length<1) {
            return;
        }
        
        // Sutvarkom didž./mažąsias raides:
        //   - standartiškai, pagal nustatytus OS lokalės parametrus;
        //   - užkomentuotas kodas [1]nustato lietuvišką lokalę ir [2]sutvarko
        //     nestandartiškai įvestus diakritikus.
        
        // Locale locale = new Locale("lt","LT");
        String[] žodžiaiDidžiosiomis = new String[žodžiai.length];
        for(int i=0; i<žodžiai.length; i++) {
            // toUpperCase() rezultatas, jei nenurodyta lokalė, priklauso nuo OS nuostatų;
            //    griežtas IDE turi generuoti įspėjimą
            žodžiaiDidžiosiomis[i] = žodžiai[i].toUpperCase(/*[1] locale*/);
            
            
            // Diskusijų objektas: normalizuoti PRIEŠ konversiją, PO TO, ar
            // DUKART?
            /*[2] žodžiaiDidžiosiomis[i] =
                Normalizer.normalize(žodžiaiDidžiosiomis[i], Normalizer.Form.NFC);*/
        }
        
        
        // Surikiuojam: rezultate pasikartojantys žodžiai bus sugrupuoti
        Arrays.sort(žodžiaiDidžiosiomis);
        
        // Atspausdiname masyvą skaičiuodami ir praleisdami pasikartojančius
        int kartojimai = 0;
        String pirmas = žodžiaiDidžiosiomis[0];
        for(String žodis: žodžiaiDidžiosiomis) {          
            if(žodis.equals(pirmas)) {
                kartojimai++;
            } else {
                System.out.printf("%s:\t%d%n", pirmas, kartojimai);
                pirmas = žodis;
                kartojimai = 1;
            }
        }
        
        System.out.printf("%s:\t%d%n", pirmas, kartojimai);
    }
    
    public static void pasikartojimųDemo(){
       
        for(String[] s:
            new String[][]{
                new String[]{ "Ąžuolas", "beržas", "ąžuolas", "ĄŽUOLAS"},
                new String[]{ },
                new String[]{ "Režimas", "Rėžimas" },
                new String[]{ "Γάμμα", "Μάγμα", "Mágma", "μαγμa", "γάμμα" },
                new String[]{ "Желать", "Лежать" },
                new String[]{ "Ave Maria, gratia plena, Dominus tecum",
                    "Virgo serena, pia, munda et immaculata" },   
            }
        ) {
            metodoAntraštė();
            žodžiųPasikartojimai(s);            
        }
            
    }

    
    /**
     * This function is prone to underflow.
     * <ul>
     * <li>Challenge1: <em>break it!</em>; NB: it has no input validation.
     *  At least 3 logically different cases.
     * <li>Challenge2: find the argument values when this function works correctly
     *  but a trivial implementation of the Pythagorean theorem does not 
     *  (implement the Pythagorean as an indepentent function).
     * <li>Challenge3: find a set of arguments that this function fails to find
     *  a correct answer. Do NOT use special values such as NaN.
     * <li>Challenge4: fix the function according to the to the tasks given
     *  by the previous Challenges.
     * <li>Challenge5: explain the magic under {@code Math.hypot()}.
     * </ul>
     * 
     * @param x coordinate of the point
     * @param y coordinate of the point
     * @param cx x of the reference
     * @param cy y of the reference
     * @param r radius of the encircled area (<em>inclusive</em>)
     * @return is the given point in the encircled area of a circle of radius r
     *      with a center at (cx,cy)
     */
    public static boolean taškasApskritime(
            final double x,
            final double y,
            final double cx,
            final double cy,
            final double r
        ) {
        return Math.hypot(x-cx, y-cy) <= r;
    }
    
    
    /**
     * Returns an approximate value of PI by MonteCarlo given a number of tests.
     * 
     * Collects the statistics of random points (x,y)∊(-1,+1)² and counts the
     * percentage of points falling into a circle (inclusive) of radius 1.
     * 
     * We know that the area 4×S(circle{diameter=1})==π×S(square{(-1,-1)(1,1)},
     * thus π is equal the percentage of points inside the circle multiplied by 4.
     * 
     * Whether the point is inside a circle is found by using {@code taškasApskritime}.
     * 
     * @param bandymųSk how many random points to draw
     * @return estimation of PI that can be calculated from the given MonteCarlo.
     */
    public static double montecarloPi(int bandymųSk) {
        if(bandymųSk<1) { throw new ArithmeticException("Negaliu spręsti pagal mažiau nei 1 bandymą ("+bandymųSk+")."); }
        
        // Atsitiktiniai parametrai X ir Y generuojami nepriklausomai
        Random randX = new Random();
        Random randY = new Random();
        
        int apskritime =  0;
        
        for(int i=0; i<bandymųSk; i++) {
            if(taškasApskritime(randX.nextDouble(), randY.nextDouble(), 0.0, 0.0, 1.0)) {
                apskritime ++;
            }
        }
        
        return (apskritime*4.0)/bandymųSk;
    }
    
    public static void montecarloPiDemo(){
        metodoAntraštė();
        
        for(int i: new int[]{ 0, 1, 4, 100, 400, 1000, 100_000, 1_000_000, 10_000_000 } ) {
            try{
                System.out.printf( "π(%d) => %f%n", i, montecarloPi(i));
            }catch(ArithmeticException e){
                System.out.println(e);
            }
        }            
    }
    
    
    public static double apskaičiuotiIšraišką(String s, Locale l) {
        Scanner sc = new Scanner(s);
        if(l != null){ sc.useLocale(l); }
        
        double a = sc.nextDouble();
        
        final String OPERATORIAUS_ŠABLONAS = "\\S+";
        
        while(sc.hasNext(OPERATORIAUS_ŠABLONAS)){
            String op = sc.next(OPERATORIAUS_ŠABLONAS);
            double b = sc.nextDouble();
        
            switch(op){
                case "+": a += b; break;
                case "-": a -= b; break;
                case "*": a *= b; break;
                case "/": a /= b; break;
                default:
                    sc.close();
                    throw new UnsupportedOperationException("Neatpažintas operatorius '"+op+"'.");
            }
        }
        sc.close();
        
        return a;
    }
    
    
    public static void išraiškųDemo(){
        metodoAntraštė();
        
        Locale l = Locale.ENGLISH;
        for(String s: new String[]{
            "  3.0E+1 / 2 + 20 ",
            "30.5 - 18 * 2",
            "2.00 - 1.50 * 1E+30",
            "0 * -1",
            "-1 / 0",
            "0 / 0",
        } ) {
            try{
                System.out.printf( "(%s) => %f%n", s, apskaičiuotiIšraišką(s,l));
            }catch(Exception e){
                System.out.printf("(%s) => %s%n", s, e);
            }
        }
        
        l = new Locale("lt");
        for(String s: new String[]{
            "3,3 + 96,7 / 100",
            "",
            "100 & 100",
        } ) {
            try{
                System.out.printf( "(%s) => %f%n", s, apskaičiuotiIšraišką(s,l));
            }catch(Exception e){
                System.out.printf("(%s) => %s%n", s, e);
            }
        }
    }
    
    public static void metodoAntraštė() {
        System.out.println("========== " +
            Thread.currentThread().getStackTrace()[2].getMethodName() +
            " ===========");
    }
    
    /**
     * Returns a number of leading zeroes in an unsigned binary number
     * @param l
     * @return 
     */
    public static int nlz(long l) {
        long t;
        int i=0;
        if((l & 0xFFFF_FFFFL) == 0L) { i+=32; t=(int)l; } else { t=(int)(l >>> 32); }
        if((t & 0x0000_FFFF) == 0) { i+=16; } else { t >>= 16; }
        if((t & 0x0000_00FF) == 0) { i+=8; } else { t >>= 8; }
        if((t & 0x0000_000F) == 0) { i+=4; } else { t >>= 2; }
        if((t & 0x0000_0003) == 0) { i+=2; } else { t >>= 1; }
        if( t != 0) { i+=1; }

        return i;
    }

    public static void commentÇaVa(String nom, String prénom){
        log.info("Comment ça va, {1} {0}?", new String[]{prénom, nom});
    }
    
    
    public static void main(String[] args) {
        MyWrappedCollection m = 
                new MyWrappedCollection(null);

        System.out.println(m.delegate);
        
        int aa = 5;
        int k = aa--; k=k - --aa;
        System.out.println(k);
        
        log.setLevel(Level.FINEST);
        log.entering();
        commentÇaVa("Hercules", "Poirot");
        commentÇaVa("René", "Descartes");        
        
        //palindromųDemo();
        //anagramųDemo();
        //pasikartojimųDemo();
        //montecarloPiDemo();
        //išraiškųDemo();
        //assert Double.valueOf(42).equals(null);

        List<Integer> a = Arrays.asList( 1,2,3,4,5 );   // varargs and implicit boxing
        List<Integer> b = Arrays.asList( 5,4,3,2,1 ); // varargs and implicit boxing
        
        int sutampa = 0;
        Iterator<Integer> itA = a.iterator();
        Iterator<Integer> itB = b.iterator();
        while(itA.hasNext()){
            while(itB.hasNext()) {
                if(itA.next().equals(itB.next())){
                    sutampa++;
                }
            }
        }
        
        List<String> l = Arrays.asList(new String[]{"a","b","c"});
        
        String sum="";
        for(String s : l ) {
            sum += sum += s;
            
            //java.text.MessageFormat.
            
            System.out.println(s);
        }
        System.out.printf("Sutampa: %d",sutampa);
        
    }

    public Lab1Uzduoteles() {
    }
    
}
