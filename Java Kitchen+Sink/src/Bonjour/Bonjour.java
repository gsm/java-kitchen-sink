/*
 * === Test ===
 * [Markdown link](info.md)
 */
package Bonjour;

import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import static java.text.MessageFormat.format;



/**
 *
 * @author Administrator
 */
public class Bonjour {
    protected static final Logger log = Logger.getLogger(Bonjour.class.getCanonicalName());
    private static final ResourceBundle STRINGS = ResourceBundle.getBundle("Bonjour");
    public static void Salut(){
        log.info(format("Bonjour, {0} {1}!", "le", "monde"));
    }
    
}
