/**
 *  Papildyti programą, kad algoritmo sudėtingumas būtų O(SPACE)=n, O(TIME)=n
 */

List<MyObject> l = new LinkedList<>();

for(int i=0; i<100_000_000; i++){
    l.add( 0, new MyObject(i) );
}

l = new ArrayList<>( l );

Random r = new Random();
for(int i=0; i<100_000_000; i++) {
    l.get( r.nextInt(l.length) );
}

